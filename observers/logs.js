/*
 * Logs-observer
 * -----------------------------------------------------------------------------
 */

var fs = require('fs');

module.exports = function(bot, from, to, text, message) {

  // For file name of logs with channels name
  var target,
      isChannel = false;
  if (to.indexOf('#') == 0) {  // A channel.
      target = to;
      isChannel = true;
  } else {  // A user.
      target = from;
  }

  // Log messages
  if (isChannel) {
      log(target, format('<%s> %s', [from, text]));
  }

  // Log IRC Channel
  function log(channel, message) {
    var filename = 'logs/%s-%s-%s-%s.txt',
        now = new Date(),
        out;
    filename = format(filename, [channel.substring(1), now.getFullYear(), now.getMonth()+1, now.getDate()]);
    out = new fs.WriteStream(filename, {
        'flags': 'a+',
        'encoding': 'utf-8',
        'mode': 0o666
    });
    var timelog = now.toTimeString().substr(0,8);
    out.write(format('%s: %s\n', [timelog, message]));
  }

  // From Django's jsi18n.
  function format(fmt, obj, named) {
    if (named) {
        return fmt.replace(/%\(\w+\)s/g, function(match){return String(obj[match.slice(2,-2)])});
    } else {
        return fmt.replace(/%s/g, function(match){return String(obj.shift())});
    }
  }

};
