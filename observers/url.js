var isChannel = require('../modules/is-channel.js');
var uregex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/;
var MetaInspector = require("node-metainspector");

module.exports = function(bot, from, to, text) {

  if (uregex.test(text) === true) {
    var result = text.match(uregex);
    var client = new MetaInspector(result[1], { timeout: 5000 });

    client.on("fetch", function(){
        var sendTo = from; // send privately
        if (isChannel(to)) {
          sendTo = to; // send publicly
        }
        bot.say(sendTo, "↳ title: " + client.title);
    });
    client.fetch();
  }

};