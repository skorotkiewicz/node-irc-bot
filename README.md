# Simple IRC Bot in Node.js

## Install
```
npm install irc request node-metainspector
```

## Configuration
Copy config_example.json to config.json and edit
```
  "server": "irc.example.com",
  "userName": "tester",
  "realName": "nodeJS IRC client",
  "port": 6667,
  "channels": ["#test"],
```

## Run
```
node app.js
```

## Systemd
Create file `/etc/systemd/system/node-irc-bot.service` and add this:

> Don't forget to change the path to script and username

```
[Service]
WorkingDirectory=/home/user/node-irc-bot/
ExecStart=/usr/bin/node /home/user/node-irc-bot/app.js
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=node-irc-bot
User=user
Environment=NODE_ENV=production

[Install]
WantedBy=multi-user.target
```


## More interactions

```javascript
// Listen for joins
bot.addListener("join", function(channel, nick, message) {
	bot.say(channel, nick + " welcome back!");
});

// Listen for any message, PM said user when he posts
bot.addListener("message", function(from, to, text, message) {
	bot.say(from, "What?");
});

// Listen for any message, say to him/her in the room
bot.addListener("message", function(from, to, text, message) {
	bot.say(config.channels[0], "Public.");
});
```
