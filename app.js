'use strict';

var   config           = require('./config.json'),
      irc              = require("irc"),
      commandHandler   = require('./modules/command-handler.js'),
      observerHandler  = require('./modules/observer-handler.js');

// Create the bot name
var bot = new irc.Client(config.server, config.userName, config);


// Check when connected to IRC server
bot.addListener("registered", function() {
    console.log("Bot is now connected with the server "+config.server);
});

// Error handler
bot.addListener('error', function(message) {
    console.log('error: ', message);
});

// Listen for messages
bot.addListener("message", function(from, to, text, message) {

    commandHandler(bot, from, to, text, message);
    observerHandler(bot, from, to, text, message);

});