var config  = require('../config.json');

/*
 * Helper to check if if channel
 * ----------------------------------------------------------------------
 */
module.exports = function(string) {

  var channelPrefixes = config.channelPrefixes.split('');

  return channelPrefixes.some(function(value) {
    return string.trim().indexOf(value) == 0;
  });

}
