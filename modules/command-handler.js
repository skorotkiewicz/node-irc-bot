var isChannel = require('../modules/is-channel.js');
var request = require("request");

/*
 * Command handler
 * ----------------------------------------------------------------------
 */

module.exports = function(bot, from, to, text, message) {

  var coreCommand = {};// lookup table for internal commands

  var opts = {
    command: String(text.split(' ')[0]).replace('!', '').trim(),
    argument: text.substring(String(text.split(' ')[0]).length).trim(),
    messageToSend: ''
  }

  /* 
   * Bot Command: !source
   * ----------------------------------------------------------------------
   */
  coreCommand.source = function() {
    bot.say(sendTo, "https://gitlab.itunix.eu/skorotkiewicz/node-irc-bot");
  };

  /*
   * Bot Command: !btc <arg>
   * ----------------------------------------------------------------------
   */
  coreCommand.btc = function(opts) {
    var arg = opts.argument;
    if ( arg == "eur" || arg == "pln" || arg == "usd" ) {
        var arg = arg.toUpperCase();
        var url = "https://api.coinbase.com/v2/prices/spot?currency=" + arg;
        var currency = arg;
    } else {
        var url = "https://api.coinbase.com/v2/prices/spot?currency=EUR";
        var currency = "EUR";
    }

    request({url: url}, function (error, response, body) {
        var amount = parseFloat(JSON.parse(body).data.amount);
        if (!error && response.statusCode === 200) {
            if ( typeof amount === "number" ) {
                bot.say(sendTo, "Bitcoin: " + amount + " " + currency);
            } else {
                bot.say(sendTo, "Bitcoin: no data");
            }
        }
    });
  };


  /*
   * Decision point
   * ----------------------------------------------------------------------
   */
  if (text && text.length > 2 && text[0] == '!') {
    var sendTo = from; // send privately
    if (isChannel(to)) {
      sendTo = to; // send publicly
    }
    // test if we have an core command for it
    if (typeof coreCommand[opts.command] === 'function') {
      coreCommand[opts.command](opts);
    }
  }
};
